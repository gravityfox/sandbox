package lol;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Fox on 9/2/2015.
 */
public class MainBogoBogoSort {

    public static Integer[] num = {/*15, 14,*/ 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    public static List<Integer> list = new ArrayList<>();

    public static void main(String[] args) {
        list.addAll(Arrays.asList(num));
        /*
        Random rand = new Random();
        for (int i = 0; i < 100; i++) {
            list.add(rand.nextInt());
        }
        */
        long time1 = System.currentTimeMillis();
        System.out.println(bogoBogoSort(list));
        long time2 = System.currentTimeMillis();
        printTime(time2 - time1);
    }

    public static List<Integer> shuffle(List<Integer> input) {
        List<Integer> output = new ArrayList<>();
        Random rand = new Random();
        while (!input.isEmpty()) {
            output.add(input.remove(rand.nextInt(input.size())));
        }
        return output;
    }

    public static List<Integer> bogoBogoSort(List<Integer> input) {
        if (input.size() <= 1) {
            return input;
        }
        List<Integer> output = new ArrayList<>();
        output.addAll(input);

        {
            List<Integer> temp = new ArrayList<>();
            temp.addAll(output);
            temp.remove(temp.size() - 1);
            temp = bogoBogoSort(temp);
            Integer last = output.get(output.size() - 1);
            output = new ArrayList<>();
            output.addAll(temp);
            output.add(last);
        }

        while (output.get(output.size() - 1) < getMax(output)) {
            output = shuffle(output);
            List<Integer> temp = new ArrayList<>();
            temp.addAll(output);
            temp.remove(temp.size() - 1);
            temp = bogoBogoSort(temp);
            Integer last = output.get(output.size() - 1);
            output = new ArrayList<>();
            output.addAll(temp);
            output.add(last);
        }
        return output;
    }

    public static List<Integer> betterSort(List<Integer> input) {
        if (input.size() <= 1) {
            return input;
        }
        List<Integer> output = new ArrayList<>();
        output.addAll(input);

        while (output.get(output.size() - 1) < getMax(output)) {
            output = shuffle(output);
        }
        List<Integer> temp = new ArrayList<>();
        temp.addAll(output);
        temp.remove(temp.size() - 1);
        temp = betterSort(temp);
        Integer last = output.get(output.size() - 1);
        output = new ArrayList<>();
        output.addAll(temp);
        output.add(last);
        return output;
    }

    public static List<Integer> bogoBogoSortVerbose(List<Integer> input, int layer) {
        printSpace(layer);
        System.out.println("v " + input);
        if (input.size() <= 1) {
            printSpace(layer);
            System.out.println(" ^ " + input);
            return input;
        }
        List<Integer> output = new ArrayList<>();
        output.addAll(input);

        {
            List<Integer> temp = new ArrayList<>();
            temp.addAll(output);
            temp.remove(temp.size() - 1);
            temp = bogoBogoSortVerbose(temp, layer + 1);
            Integer last = output.get(output.size() - 1);
            output = new ArrayList<>();
            output.addAll(temp);
            output.add(last);
        }

        while (output.get(output.size() - 1) < getMax(output)) {
            output = shuffle(output);
            List<Integer> temp = new ArrayList<>();
            temp.addAll(output);
            temp.remove(temp.size() - 1);
            temp = bogoBogoSortVerbose(temp, layer + 1);
            Integer last = output.get(output.size() - 1);
            output = new ArrayList<>();
            output.addAll(temp);
            output.add(last);
        }
        printSpace(layer);
        System.out.println(" ^ " + output);
        return output;
    }

    public static int getMax(List<Integer> input) {
        int output = Integer.MIN_VALUE;
        for (Integer integer : input) {
            output = integer > output ? integer : output;
        }
        return output;
    }

    public static void printSpace(int number) {
        for (int i = 0; i < number; i++) {
            System.out.print(" ");
        }
    }

    public static void printTime(long millis) {
        System.out.println(String.format("%03d:%02d:%02d:%02d:%03d",
                TimeUnit.MILLISECONDS.toDays(millis),
                TimeUnit.MILLISECONDS.toHours(millis) -
                        TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)), millis % 1000));
    }
}
