package net.ddns.superfox.school.sandbox;


/**
 * Created by Fox on 9/7/2015.
 */
public class FoxIntegerLinkedList {

    private Node first;
    private Node last;

    private int size = 0;

    private static class Node {
        Integer item;
        Node prev;
        Node next;

        Node(Node prev, Integer element, Node next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    FoxIntegerLinkedList() {

    }

    public void add(Integer par1Input) {
        final Node l = last;
        final Node newNode = new Node(l, par1Input, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
    }

    public void add(Integer par1Input, int par2Index){
        if(par2Index > size) return;
        final Node prevNode = getNode(par2Index-1);
        final Node nextNode = getNode(par2Index);
        final Node newNode = new Node(prevNode, par1Input, nextNode);
        if(prevNode == null)
            first = newNode;
        else
            prevNode.next = nextNode;
        if (nextNode == null)
            last = newNode;
        else
            nextNode.prev = nextNode;
        size++;
    }

    public Integer get(int index) {
        return getNode(index).item;
    }
    
    public Integer remove(int index) {
        final Node x = getNode(index);
        if(x == null) return null;
        final Integer element = x.item;
        final Node next = x.next;
        final Node prev = x.prev;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.item = null;
        size--;
        return element;
    }

    public int size() {
        return this.size;
    }

    private Node getNode(int index) {
        if (index >= size || index < 0) return null;
        Node x = this.first;
        for (int i = 0; i < index; i++)
            x = x.next;
        return x;
    }

}
