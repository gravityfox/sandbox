package net.ddns.superfox.school.sandbox;

import java.util.*;

/**
 * Created by Fox on 8/26/2015.
 */
public class MainLists {


    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            test();
        }
    }

    public static void test() {
        long time1, time2;
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();
        preRoutine(arrayList);
        preRoutine(linkedList);
        time1 = System.currentTimeMillis();
        routine(arrayList);
        time2 = System.currentTimeMillis();
        System.out.println("ArrayList: " + (time2 - time1));
        time1 = System.currentTimeMillis();
        routine(linkedList);
        time2 = System.currentTimeMillis();
        System.out.println("LinkedList: " + (time2 - time1));

    }

    public static void preRoutine(List<Integer> list) {
        fill(list);
    }


    public static void routine(List<Integer> list) {
        Collections.sort(list);
    }

    public static void fill(List<Integer> list) {
        Random ran = new Random();
        for (int i = 0; i < 10000000; i++) {
            list.add(ran.nextInt());
        }
    }
}
