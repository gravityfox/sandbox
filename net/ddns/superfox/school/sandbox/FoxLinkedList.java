package net.ddns.superfox.school.sandbox;


/**
 * Created by Fox on 9/7/2015.
 */
public class FoxLinkedList<E> {

    private Node<E> first;
    private Node<E> last;

    private int size = 0;

    private static class Node<E> {
        E item;
        Node<E> prev;
        Node<E> next;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    FoxLinkedList() {

    }

    public void add(E par1Input) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, par1Input, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
    }
    
    public void add(E par1Input, int par2Index){
        if(par2Index > size) return;
        final Node<E> prevNode = getNode(par2Index-1);
        final Node<E> nextNode = getNode(par2Index);
        final Node<E> newNode = new Node<>(prevNode, par1Input, nextNode);
        if(prevNode == null)
            first = newNode;
        else
            prevNode.next = nextNode;
        if (nextNode == null)
            last = newNode;
        else
            nextNode.prev = nextNode;
        size++;
    }

    public E get(int index) {
        return getNode(index).item;
    }
    
    public E remove(int index) {
        final Node<E> x = getNode(index);
        if(x == null) return null;
        final E element = x.item;
        final Node<E> next = x.next;
        final Node<E> prev = x.prev;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.item = null;
        size--;
        return element;
    }

    public int size() {
        return this.size;
    }

    private Node<E> getNode(int index) {
        if (index >= size || index < 0) return null;
        Node<E> x = this.first;
        for (int i = 0; i < index; i++)
            x = x.next;
        return x;
    }

}
